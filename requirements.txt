Flask==2.0.1
SQLAlchemy==1.3.24
Werkzeug==2.0.1
Flask_Migrate==3.0.1
Flask_SQLAlchemy==2.5.1
psycopg2-binary==2.9.1