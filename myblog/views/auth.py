import functools

from flask import (
    Blueprint, render_template, request, redirect, url_for, flash, session, g
)

from werkzeug.security import check_password_hash, generate_password_hash

from myblog.models.db import db
from myblog.models.user import User

auth_bp = Blueprint('auth_bp', __name__, url_prefix='/auth')


@auth_bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
        elif db.session.query(User). \
                filter(User.name == username). \
                first() is not None:
            error = f"User {username} is already registered."

        if error is None:
            user = User(
                name=username,
                password=generate_password_hash(password)
            )
            db.session.add(user)
            db.session.commit()
            return redirect(url_for('auth_bp.login'))

        flash(error)

    return render_template('auth/register.html')


@auth_bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        error = None
        user = db.session.query(User). \
            with_entities(User.id, User.name, User.password). \
            filter(User.name == username). \
            first()

        if user is None:
            error = 'Неверное имя пользователя.'
        elif not check_password_hash(user.password, password):
            error = 'Неверный пароль.'

        if error is None:
            session.clear()
            session['user_id'] = user.id
            return redirect(url_for('posts_bp.list'))

        flash(error)

    return render_template('auth/login.html')


@auth_bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('posts_bp.list'))


@auth_bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = db.session.query(User). \
            filter(User.id == user_id). \
            first()


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth_bp.login'))
        return view(**kwargs)
    return wrapped_view
