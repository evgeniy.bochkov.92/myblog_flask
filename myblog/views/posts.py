from flask import Blueprint, render_template, request, url_for, redirect, flash, g
from sqlalchemy import desc

from werkzeug.exceptions import NotFound, abort

from myblog.models.db import db
from myblog.models.post import Post
from myblog.models.user import User
from myblog.views.auth import login_required

posts_bp = Blueprint('posts_bp', __name__, url_prefix='/posts')


@posts_bp.route('/', endpoint='list')
def posts_list():
    posts = db.session.query(Post). \
        join(User). \
        with_entities(Post.id, Post.title, Post.created_at,
                      Post.description,
                      User.name.label('username')). \
        order_by(desc(Post.created_at)). \
        all()
    return render_template(
        'posts/posts_list.html', posts=posts)


@posts_bp.route('/<int:post_id>/', endpoint='details')
def post_details(post_id):
    post = db.session.query(Post). \
        join(User). \
        with_entities(Post.id, Post.title, Post.created_at,
                      Post.description, Post.body,
                      User.id.label('user_id'),
                      User.name.label('username')). \
        filter(Post.id == post_id). \
        first()
    if post is None:
        raise NotFound(f"Пост #{post_id} не найден!")
    return render_template(
        'posts/post_details.html', post=post)


@posts_bp.route("/create/", methods=("GET", "POST"), endpoint="create")
@login_required
def create_post():
    if request.method == "POST":
        post_title = request.form.get("post-title")
        post_description = request.form.get("post-description")
        post_body = request.form.get("post-body")
        error = None
        if not post_title:
            error = 'Требуется заполнить заголовок поста'
        elif not post_description:
            error = 'Требуется заполнить описание поста.'
        elif not post_body:
            error = 'Требуется заполнить текст поста.'
        if error is None:
            post = Post(
                title=post_title,
                description=post_description,
                body=post_body,
                user_id=g.user.id
            )
            db.session.add(post)
            db.session.flush()

            post_id = post.id

            db.session.commit()
            return redirect(
                url_for("posts_bp.details", post_id=post_id))
        flash(error)
    return render_template("posts/post_create.html")


def get_post(post_id, check_author=True):
    post = db.session.query(Post). \
        join(User). \
        with_entities(Post.id, Post.title,
                      Post.description, Post.body,
                      User.id.label('user_id')). \
        filter(Post.id == post_id). \
        first()
    if post is None:
        raise NotFound(f"Пост #{post_id} не найден!")

    # if post is None:
    #   abort(404, "Post id {0} doesn't exist.".format(post_id))

    if check_author and post.user_id != g.user.id:
        abort(403)
    return post


@posts_bp.route('/<int:post_id>/update/', methods=('GET', 'POST'), endpoint='update')
@login_required
def update(post_id):
    post = get_post(post_id)

    if request.method == 'POST':
        post_title = request.form.get("post-title")
        post_description = request.form.get("post-description")
        post_body = request.form.get("post-body")
        error = None
        if not post_title:
            error = 'Требуется заполнить заголовок поста'
        elif not post_description:
            error = 'Требуется заполнить описание поста.'
        elif not post_body:
            error = 'Требуется заполнить текст поста.'

        if error is not None:
            flash(error)
        else:
            db.session.query(Post).filter(Post.id == post_id). \
                update({
                    Post.title: post_title,
                    Post.description: post_description,
                    Post.body: post_body
                }, synchronize_session=False)
            db.session.commit()
            return redirect(
                url_for("posts_bp.details", post_id=post_id))

    return render_template('posts/post_update.html', post=post)


@posts_bp.route('/<int:post_id>/delete/', methods=('POST',))
@login_required
def delete(post_id):
    get_post(post_id)
    db.session.query(Post).filter(Post.id == post_id). \
        delete(synchronize_session=False)
    db.session.commit()
    return redirect(url_for('posts_bp.list'))
