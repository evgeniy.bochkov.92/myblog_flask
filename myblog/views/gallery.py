from flask import Blueprint, render_template

gallery_bp = Blueprint('gallery_bp', __name__, url_prefix='/gallery')


@gallery_bp.route('/', endpoint='main')
def gallery():
    return render_template(
        'gallery/gallery.html')
