from flask import Flask, redirect, url_for
from flask_migrate import Migrate
from werkzeug.utils import import_string

from myblog.models.db import db
from myblog.views.posts import posts_bp
from myblog.views.gallery import gallery_bp
from myblog.views.auth import auth_bp

app = Flask(__name__, static_url_path='/myblog/static')
cfg = import_string("myblog.config.ProductionConfig")()
app.config.from_object(cfg)

db.init_app(app)
migrate = Migrate(app, db)

app.register_blueprint(posts_bp)
app.register_blueprint(gallery_bp)
app.register_blueprint(auth_bp)


@app.route('/')
def index():
    return redirect(url_for('posts_bp.list'))
