from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, func, ForeignKey, Text
from sqlalchemy.orm import relationship

from myblog.models.db import db
from myblog.models.post_tag import post_tag


class Post(db.Model):
    __tablename__ = "posts"
    id = Column(Integer, primary_key=True)
    title = Column(String(70), nullable=False, default="", server_default="")
    created_at = Column(DateTime, nullable=False, default=datetime.utcnow, server_default=func.now())
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    body = Column(Text, nullable=False, default="", server_default="")
    description = Column(String(155), nullable=False, default="", server_default="")

    tags = relationship(
        "Tag",
        secondary=post_tag,
        back_populates="posts"
    )
    author = relationship("User", back_populates="posts")

    def __repr__(self):
        return f"{self.title}"
