from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from myblog.models.db import db
from myblog.models.post_tag import post_tag


class Tag(db.Model):
    __tablename__ = "tags"
    id = Column(Integer, primary_key=True)
    name = Column(String(30), unique=True, default="", server_default="")

    posts = relationship(
        "Post",
        secondary=post_tag,
        back_populates="tags"
    )
