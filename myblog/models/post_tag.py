from sqlalchemy import Table, Column, Integer, ForeignKey

from myblog.models.db import db

post_tag = Table(
    "post_tag", db.Model.metadata,
    Column("post_id", Integer, ForeignKey("posts.id")),
    Column("tag_id", Integer, ForeignKey("tags.id"))
)
