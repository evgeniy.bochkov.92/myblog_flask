from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from myblog.models.db import db


class UserProfile(db.Model):
    __tablename__ = "user_profiles"
    id = Column(Integer, primary_key=True)
    first_name = Column(String(50), nullable=False, default="", server_default="")
    last_name = Column(String(50), nullable=False, default="", server_default="")
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False, unique=True)

    user = relationship("User", back_populates="profile")
