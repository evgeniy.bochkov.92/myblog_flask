class Config(object):
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = b'*\xa8Vs\x8bG\t\xcb\x1c\xc4\x05z0nSh'

    @property
    def SQLALCHEMY_DATABASE_URI(self):  # Note: all caps
        return f'postgresql+psycopg2://postgres:postgres@' \
               f'{format(self.DB_SERVER)}' \
               f':5432/myblog'


class ProductionConfig(Config):
    DB_SERVER = 'db'


class DevelopmentConfig(Config):
    DEBUG = True
    DEVELOPMENT = True
    DB_SERVER = 'localhost'
